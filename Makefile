.PHONY: delete-resources apply-resources redeploy

del:
	@echo "Deleting existing dev namespace and its resources..."
	kubectl delete namespace dev
	kubectl delete pvc --all -n dev
	kubectl delete all --all -n dev

apply:
	@echo "Applying new configurations..."
	kubectl apply -f deploy/dev-namespace.yaml
	kubectl apply -f deploy/network-policies.yaml -n dev
	kubectl apply -f deploy/configmap.yaml -f deploy/secret.yaml -n dev
	kubectl apply -f deploy/mongodb.yaml -f deploy/zookeeper.yaml -n dev
	kubectl apply -f deploy/url-shortener.yaml -n dev

redeploy: del apply
	@echo "Redeployment completed!"
