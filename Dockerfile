#build image to build the projects
FROM maven:3.8.4-openjdk-17 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package -DskipTests

#copy jar from build image
FROM openjdk:17
COPY --from=build /usr/src/app/target/url-shortener-0.0.1-SNAPSHOT.jar /usr/app/url-shortener.jar

CMD ["/bin/sh", "-c", "/usr/bin/java -Dspring.profiles.active=${SPRING_PROFILE_ACTIVE} -jar /usr/app/url-shortener.jar"]