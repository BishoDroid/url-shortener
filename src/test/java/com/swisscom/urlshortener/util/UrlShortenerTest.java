package com.swisscom.urlshortener.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class UrlShortenerTest {

    private UrlShortener urlShortener;

    @BeforeEach
    public void setUp() {
        urlShortener = new UrlShortener();
        ReflectionTestUtils.setField(urlShortener, "prefix", "https://localhost:8443/");
    }

    @Test
    public void shortenUrl_ShouldGenerateExpectedLength() throws NoSuchAlgorithmException {
        String originalUrl = "http://www.example.com";
        long counter = 100L;

        String result = urlShortener.shortenUrl(originalUrl, counter);

        assertNotNull(result);
        assertEquals(8, result.length());
    }

    @Test
    public void prefixedShortUrl_ShouldConcatenatePrefixAndShortUrl() {
        String shortUrl = "abcd1234";

        String result = urlShortener.prefixedShortUrl(shortUrl);

        assertEquals("https://localhost:8443/abcd1234", result);
    }

    @Test
    public void shortenUrl_WithSameInput_ShouldGenerateSameOutput() throws NoSuchAlgorithmException {
        String originalUrl = "http://www.example.com";
        long counter = 100L;

        String result1 = urlShortener.shortenUrl(originalUrl, counter);
        String result2 = urlShortener.shortenUrl(originalUrl, counter);

        assertEquals(result1, result2);
    }

    @Test
    public void shortenUrl_WithDifferentInput_ShouldGenerateDifferentOutput() throws NoSuchAlgorithmException {
        String originalUrl1 = "http://www.example.com";
        String originalUrl2 = "http://www.different.com";
        long counter = 100L;

        String result1 = urlShortener.shortenUrl(originalUrl1, counter);
        String result2 = urlShortener.shortenUrl(originalUrl2, counter);

        assertNotEquals(result1, result2);
    }

    @Test
    public void shortenUrl_WithDifferentCounter_ShouldGenerateDifferentOutput() throws NoSuchAlgorithmException {
        String originalUrl = "http://www.example.com";
        long counter0 = 20L;
        long counter = 100L;

        String result1 = urlShortener.shortenUrl(originalUrl, counter0);
        String result2 = urlShortener.shortenUrl(originalUrl, counter);

        assertNotEquals(result1, result2);
    }

}
