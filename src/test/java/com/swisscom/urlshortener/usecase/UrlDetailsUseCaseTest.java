package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.IAuthenticationFacade;
import com.swisscom.urlshortener.dto.request.UrlDetailsRequest;
import com.swisscom.urlshortener.dto.response.UrlDetailsResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UrlRepository;
import com.swisscom.urlshortener.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UrlDetailsUseCaseTest {

    @Mock
    private UrlRepository urlRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private IAuthenticationFacade authenticationFacade;

    @InjectMocks
    private UrlDetailsUseCase urlDetailsUseCase;

    private final String shortUrl = "ab12cd34";
    private final String username = "user";
    private Authentication authentication;

    @BeforeEach
    public void setUp() {
        authentication = mock(Authentication.class);
    }

    @Test
    public void whenShortUrlIsBlank_thenThrowMissingShortUrlException() {
        UrlDetailsRequest request = new UrlDetailsRequest(UUID.randomUUID().toString(), "");

        AppException exception = assertThrows(AppException.class, () -> {
            urlDetailsUseCase.validateRequest(request);
        });

        assertEquals(AppError.MISSING_SHORT_URL, exception.getError());
    }

    @Test
    public void whenUserNotFound_thenThrowAppException() {
        UrlDetailsRequest request = new UrlDetailsRequest(UUID.randomUUID().toString(),shortUrl);

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());
        when(authenticationFacade.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn(username);

        AppException exception = assertThrows(AppException.class, () -> {
            urlDetailsUseCase.runUseCaseLogic(request);
        });

        assertEquals(AppError.USER_NOT_FOUND, exception.getError());
    }

    @Test
    public void whenUserNotMatching_thenThrowAppException() {
        UrlDetailsRequest request = new UrlDetailsRequest(UUID.randomUUID().toString(),shortUrl);
        User user = User.builder().username(username).build();
        User differentUser = User.builder().username("differentUser").build();
        Url url = Url.builder().shortUrl(shortUrl).user(differentUser).build();

        when(authenticationFacade.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(urlRepository.findOneByShortUrl(shortUrl)).thenReturn(Optional.of(url));

        AppException exception = assertThrows(AppException.class, () -> {
            urlDetailsUseCase.runUseCaseLogic(request);
        });

        assertEquals(AppError.USER_NOT_MATCHING, exception.getError());
    }

    @Test
    public void whenValidRequest_thenReturnUrlDetails() throws AppException {
        UrlDetailsRequest request = new UrlDetailsRequest(UUID.randomUUID().toString(),shortUrl);
        User user = User.builder().username(username).build();
        String prefix = "https://sho.rt/";
        Url url = Url.builder().prefix(prefix).shortUrl(shortUrl).user(user).build();

        when(authenticationFacade.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(urlRepository.findOneByShortUrl(shortUrl)).thenReturn(Optional.of(url));

        UrlDetailsResponse response = urlDetailsUseCase.runUseCaseLogic(request);

        assertNotNull(response);
        assertEquals(url.getPrefixedUrl(), response.getShortUrl());
    }
}
