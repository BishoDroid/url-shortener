package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.dto.request.ShortenUrlRequest;
import com.swisscom.urlshortener.dto.response.ShortenUrlResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UrlRepository;
import com.swisscom.urlshortener.repository.UserRepository;
import com.swisscom.urlshortener.util.UrlShortener;
import com.swisscom.urlshortener.util.ZooKeeperRangeManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ShortenUrlUseCaseTest {

    @Mock
    private UrlRepository urlRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ZooKeeperRangeManager rangeManager;

    private final UrlShortener urlShortener = new UrlShortener();
    @InjectMocks
    private ShortenUrlUseCase shortenUrlUseCase;


    private final String validUrl = "http://www.example.com";
    private final String username = "user";

    @BeforeEach
    public void setUp() throws NoSuchAlgorithmException {
        shortenUrlUseCase = new ShortenUrlUseCase(urlRepository, userRepository, rangeManager, urlShortener);
    }

    @Test
    public void whenValidUrlIsProvided_thenShortenAndReturnSuccess() throws NoSuchAlgorithmException, AppException {
        ShortenUrlRequest request = new ShortenUrlRequest(UUID.randomUUID().toString(),validUrl, username);
        User user = User.builder().build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(urlRepository.existsByShortUrl(anyString())).thenReturn(false);
        when(rangeManager.getNextCounter()).thenReturn(1L);

        ShortenUrlResponse response = shortenUrlUseCase.runUseCaseLogic(request);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatus());
        assertEquals(validUrl, response.getOriginalUrl());
        assertNotNull(response.getShortUrl());
        verify(urlRepository).save(any(Url.class));
    }

    @Test
    public void whenInvalidUrlIsProvided_thenThrowAppException() {
        String invalidUrl = "htt:/invalid-url";
        ShortenUrlRequest request = new ShortenUrlRequest(UUID.randomUUID().toString(), invalidUrl, username);

        AppException exception = assertThrows(AppException.class, () -> {
            shortenUrlUseCase.validateRequest(request);
        });

        assertEquals(AppError.INVALID_URL_RECEIVED, exception.getError());
    }

    @Test
    public void whenUsernameNotFound_thenThrowAppException() {
        ShortenUrlRequest request = new ShortenUrlRequest(UUID.randomUUID().toString(), validUrl, username);

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        AppException exception = assertThrows(AppException.class, () -> {
            shortenUrlUseCase.runUseCaseLogic(request);
        });

        assertEquals(AppError.USER_NOT_FOUND, exception.getError());
    }

    @Test
    public void whenMaxRetriesExceeded_thenThrowAppException() throws NoSuchAlgorithmException, AppException {
        ShortenUrlRequest request = new ShortenUrlRequest(UUID.randomUUID().toString(),validUrl, username);
        User user = User.builder().build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(urlRepository.existsByShortUrl(anyString())).thenReturn(true);
        when(rangeManager.getNextCounter()).thenReturn(1L);

        AppException exception = assertThrows(AppException.class, () -> {
            shortenUrlUseCase.runUseCaseLogic(request);
        });

        assertEquals(AppError.UNABLE_TO_SHORTEN, exception.getError());
    }
}
