package com.swisscom.urlshortener.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class AppException extends RuntimeException {

    AppError error;

    public AppException(AppError error) {
        super(error.getMessage());
        this.error = error;
    }

}
