package com.swisscom.urlshortener.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum AppError {

    USER_NOT_FOUND("User not found", HttpStatus.NOT_FOUND),
    USER_EMAIL_EXISTS("User email already exists", HttpStatus.BAD_REQUEST),
    USER_NAME_EXISTS("Username already exists", HttpStatus.BAD_REQUEST),
    INVALID_TOKEN("Given token is not valid. Please login again", HttpStatus.BAD_REQUEST),
    INVALID_TOKEN_SUBJECT("Invalid refresh token subject", HttpStatus.UNAUTHORIZED),
    EXPIRED_REFRESH_TOKEN("Refresh token has expired. Please login again", HttpStatus.UNAUTHORIZED),
    EXPIRED_TOKEN("Given token has expired. Please login again", HttpStatus.UNAUTHORIZED),
    INVALID_URL_RECEIVED("Invalid URL received", HttpStatus.BAD_REQUEST),
    UNABLE_TO_SHORTEN("Unable to shorten URL", HttpStatus.INTERNAL_SERVER_ERROR),
    URL_NOT_FOUND("URL not found", HttpStatus.NOT_FOUND),
    MISSING_SHORT_URL("You must pass shortUrl", HttpStatus.BAD_REQUEST),
    USER_NOT_MATCHING("This URL does not belong to this user", HttpStatus.FORBIDDEN),
    URL_NOT_ACTIVE("Url is not active", HttpStatus.BAD_GATEWAY),
    REQUEST_PARAMS_MISSING("Request body is required items. check the request and try again", HttpStatus.BAD_REQUEST),
    SERVER_ERROR("An error occurred while processing your request. Please contact admin if this issue persists.", HttpStatus.INTERNAL_SERVER_ERROR);

    final String message;
    final HttpStatus status;
}
