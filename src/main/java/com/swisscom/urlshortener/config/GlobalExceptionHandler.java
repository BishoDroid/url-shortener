package com.swisscom.urlshortener.config;

import com.swisscom.urlshortener.dto.response.BaseResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({AppException.class})
    public ResponseEntity<BaseResponse> handleAppException(AppException ex) {
        log.info("Handling error: {}", ex.getMessage());
        BaseResponse errorResponse = BaseResponse.builder().status(ex.getError().getStatus())
                .message(ex.getError().getMessage()).build();
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }


    // General exception handler as a catch-all
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<BaseResponse> handleException(Exception ex) {
        log.error("An error occurred while processing request. {}", ex.getMessage());

        BaseResponse errorResponse = BaseResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR).message("An error occurred while processing request")
                .build();
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }

}

