package com.swisscom.urlshortener.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    private String jwtSecret;
    private String jwtRefreshSecret;
    private int jwtExpiration; // in minutes
    private int jwtRefreshExpiration; // in minutes
    private String rangeBasePath;
}
