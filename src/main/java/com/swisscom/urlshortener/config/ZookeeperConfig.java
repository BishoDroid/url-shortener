package com.swisscom.urlshortener.config;

import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZookeeperConfig {

    @Value("${spring.cloud.zookeeper.connect-string}")
    private String zookeeperAddress;
    private static final int SESSION_TIMEOUT = 3000;

    @Bean
    public ZooKeeper zooKeeper() throws Exception {
        return new ZooKeeper(zookeeperAddress, SESSION_TIMEOUT, watchedEvent -> {});
    }
}
