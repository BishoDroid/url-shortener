package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.exception.AppException;

public abstract class AbstractUseCase<Request, Response> {

    public Response execute(Request request) throws AppException {
        validateRequest(request);
        return runUseCaseLogic(request);
    }

    protected abstract void validateRequest(Request request) throws AppException;

    protected abstract Response runUseCaseLogic(Request request) throws AppException;
}


