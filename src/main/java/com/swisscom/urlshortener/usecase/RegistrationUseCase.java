package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.dto.request.RegistrationRequest;
import com.swisscom.urlshortener.dto.response.RegistrationResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegistrationUseCase extends AbstractUseCase<RegistrationRequest, RegistrationResponse> {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void validateRequest(RegistrationRequest request) throws AppException {
        if(userRepository.existsByEmail(request.getEmail())) {
            throw new AppException(AppError.USER_EMAIL_EXISTS);
        }
        if(userRepository.existsByUsername(request.getUsername())) {
            throw  new AppException(AppError.USER_NAME_EXISTS);
        }
    }

    @Override
    protected RegistrationResponse runUseCaseLogic(RegistrationRequest request) throws AppException {
        log.info("action={}, requestId={}, message={}",
                "registration",
                request.getRequestId(),
                MessageFormat.format("Registering user {0} with email {1}",
                        request.getUsername(), request.getEmail()));

        User newUser = userRepository.save(User.builder()
                .email(request.getEmail())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .roles(Set.of("USER"))
                .build());

        return RegistrationResponse.fromUser(newUser);
    }
}
