package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.config.AppConfig;
import com.swisscom.urlshortener.dto.request.ShortenUrlRequest;
import com.swisscom.urlshortener.dto.response.ShortenUrlResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UrlRepository;
import com.swisscom.urlshortener.repository.UserRepository;
import com.swisscom.urlshortener.util.UrlShortener;
import com.swisscom.urlshortener.util.ZooKeeperRangeManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShortenUrlUseCase extends AbstractUseCase<ShortenUrlRequest, ShortenUrlResponse> {
    private static final String URL_REGEX =
            "^https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)$";
    private static final Pattern URL_PATTERN = Pattern.compile(URL_REGEX);
    private static final int MAX_RETRIES = 10;
    @Value("${app.prefix}")
    private String prefix;

    private final UrlRepository urlRepository;
    private final UserRepository userRepository;
    private final ZooKeeperRangeManager rangeManager;
    private final UrlShortener urlShortener;

    @Override
    protected void validateRequest(ShortenUrlRequest shortenUrlRequest) throws AppException {
        if(!isValidUrl(shortenUrlRequest.getUrl())) {
            log.error("Invalid url received {}", shortenUrlRequest.getUrl());
            throw new AppException(AppError.INVALID_URL_RECEIVED);
        }
    }

    @Override
    protected ShortenUrlResponse runUseCaseLogic(ShortenUrlRequest shortenUrlRequest) throws AppException {
        log.info(shortenUrlRequest.toString());
        User user = userRepository.findByUsername(shortenUrlRequest.getUsername())
                .orElseThrow(() -> new AppException(AppError.USER_NOT_FOUND));
        try {
            for(int i=0; i < MAX_RETRIES; i++) {
                String shortUrl = urlShortener.shortenUrl(shortenUrlRequest.getUrl(), rangeManager.getNextCounter());
                if(!urlRepository.existsByShortUrl(shortUrl)) {
                    Url url = Url.builder()
                            .longUrl(shortenUrlRequest.getUrl())
                            .active(true)
                            .clicks(0)
                            .prefix(prefix)
                            .shortUrl(shortUrl)
                            .user(user)
                            .build();
                    urlRepository.save(url);
                    return ShortenUrlResponse.builder()
                            .message("URL shortened successfully")
                            .status(HttpStatus.OK)
                            .originalUrl(shortenUrlRequest.getUrl())
                            .shortUrl(urlShortener.prefixedShortUrl(shortUrl))
                            .build();
                }
            }
            throw new AppException(AppError.UNABLE_TO_SHORTEN);
        } catch (NoSuchAlgorithmException e) {
            log.error("An error occurred while shortening the URL. {}", e.getMessage());
            throw new AppException(AppError.SERVER_ERROR);
        }
    }

    private boolean isValidUrl(String url) {
        return URL_PATTERN.matcher(url).matches();
    }
}
