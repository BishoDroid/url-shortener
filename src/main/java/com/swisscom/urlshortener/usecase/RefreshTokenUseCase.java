package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.AuthTokenService;
import com.swisscom.urlshortener.dto.request.RefreshTokenRequest;
import com.swisscom.urlshortener.dto.response.RefreshTokenResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RefreshTokenUseCase extends AbstractUseCase<RefreshTokenRequest, RefreshTokenResponse> {

    private final AuthTokenService authTokenService;

    @Override
    protected void validateRequest(RefreshTokenRequest refreshTokenRequest) throws AppException {
        if(!authTokenService.refreshTokenExists(refreshTokenRequest.getRefreshToken())) {
            throw new AppException(AppError.INVALID_TOKEN);
        }
    }

    @Override
    protected RefreshTokenResponse runUseCaseLogic(RefreshTokenRequest refreshTokenRequest) throws AppException {

        AuthTokenService.Token accessToken = authTokenService.generateFromRefreshToken(refreshTokenRequest.getRefreshToken());

        return RefreshTokenResponse.builder()
                .accessToken(accessToken.getToken())
                .accessTokenExpiresIn(accessToken.getExpiresIn())
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .build();
    }
}
