package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.IAuthenticationFacade;
import com.swisscom.urlshortener.dto.request.BaseRequest;
import com.swisscom.urlshortener.dto.response.AllUrlDetailsResponse;
import com.swisscom.urlshortener.dto.response.UrlDetailsResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UrlRepository;
import com.swisscom.urlshortener.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AllUrlDetailsUseCase extends AbstractUseCase<BaseRequest, AllUrlDetailsResponse> {

    private final UrlRepository urlRepository;
    private final UserRepository userRepository;
    private final IAuthenticationFacade authenticationFacade;

    @Override
    protected void validateRequest(BaseRequest baseRequest) throws AppException {

    }

    @Override
    protected AllUrlDetailsResponse runUseCaseLogic(BaseRequest baseRequest) throws AppException {
        String username = authenticationFacade.getAuthentication().getName();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new AppException(AppError.USER_NOT_FOUND));

        List<Url> urls = urlRepository.findByUser(user)
                .orElse(Collections.emptyList());

        List<UrlDetailsResponse> urlsDetails = urls.stream()
                .map(UrlDetailsResponse::fromModel)
                .toList();
        return AllUrlDetailsResponse.builder()
                .urlsDetails(urlsDetails)
                .message("Retrieved Url details")
                .status(HttpStatus.OK)
                .build();
    }
}
