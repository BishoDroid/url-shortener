package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.IAuthenticationFacade;
import com.swisscom.urlshortener.dto.request.UrlStatusRequest;
import com.swisscom.urlshortener.dto.response.UrlStatusResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.repository.UrlRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UrlStatusUseCase extends AbstractUseCase<UrlStatusRequest, UrlStatusResponse> {

    private final UrlRepository urlRepository;
    private final IAuthenticationFacade authenticationFacade;

    @Override
    protected void validateRequest(UrlStatusRequest request) throws AppException {
        if(request.getShortUrl().isEmpty()) {
            throw new AppException(AppError.REQUEST_PARAMS_MISSING);
        }
    }

    @Override
    protected UrlStatusResponse runUseCaseLogic(UrlStatusRequest request) throws AppException {
        Url url = urlRepository.findOneByShortUrl(request.getShortUrl())
                .orElseThrow(() -> new AppException(AppError.URL_NOT_FOUND));

        if(!url.getUser().getUsername().equals(authenticationFacade.getAuthentication().getName())) {
            throw new AppException(AppError.USER_NOT_MATCHING);
        }

        if(url.isActive() && request.isActive()) {
            return UrlStatusResponse.builder()
                    .message("Urls status unchanged")
                    .status(HttpStatus.NOT_MODIFIED)
                    .shortUrl(url.getShortUrl())
                    .build();
        }
        url.setActive(request.isActive());
        urlRepository.save(url);
        return UrlStatusResponse.builder()
                .message("Url status changed")
                .status(HttpStatus.OK)
                .build();
    }
}
