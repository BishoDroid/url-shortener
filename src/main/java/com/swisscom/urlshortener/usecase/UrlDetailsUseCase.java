package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.IAuthenticationFacade;
import com.swisscom.urlshortener.dto.request.UrlDetailsRequest;
import com.swisscom.urlshortener.dto.response.UrlDetailsResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import com.swisscom.urlshortener.repository.UrlRepository;
import com.swisscom.urlshortener.repository.UserRepository;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UrlDetailsUseCase extends AbstractUseCase<UrlDetailsRequest, UrlDetailsResponse>{

    private final UrlRepository urlRepository;
    private final UserRepository userRepository;
    private final IAuthenticationFacade authenticationFacade;

    @Override
    protected void validateRequest(UrlDetailsRequest urlDetailsRequest) throws AppException {
        if(StringUtils.isBlank(urlDetailsRequest.getShortUrl())) {
            throw new AppException(AppError.MISSING_SHORT_URL);
        }
    }

    @Override
    protected UrlDetailsResponse runUseCaseLogic(UrlDetailsRequest urlDetailsRequest) throws AppException {
        log.info("Retrieving url details for shortUrl {}", urlDetailsRequest.getShortUrl());

        String username = authenticationFacade.getAuthentication().getName();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new AppException(AppError.USER_NOT_FOUND));

        Url url = urlRepository.findOneByShortUrl(urlDetailsRequest.getShortUrl())
                .orElseThrow(() -> new AppException(AppError.URL_NOT_FOUND));

        if(!url.getUser().getUsername().equals(user.getUsername())) {
            throw new AppException(AppError.USER_NOT_MATCHING);
        }
        return UrlDetailsResponse.fromModel(url);
    }
}
