package com.swisscom.urlshortener.usecase;

import com.swisscom.urlshortener.auth.AuthTokenService;
import com.swisscom.urlshortener.auth.UserDetailsImpl;
import com.swisscom.urlshortener.dto.request.LoginRequest;
import com.swisscom.urlshortener.dto.response.LoginResponse;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginUseCase extends AbstractUseCase<LoginRequest, LoginResponse>{

    private final UserRepository userRepository;
    private final AuthTokenService authTokenService;
    private final AuthenticationManager authenticationManager;

    @Override
    protected void validateRequest(LoginRequest request) throws AppException {
        log.info("action: {}, username: {}", "login_request_validation", request.getUsername());
        userRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> new AppException(AppError.USER_NOT_FOUND));
    }

    @Override
    protected LoginResponse runUseCaseLogic(LoginRequest request) throws AppException {

        AuthTokenService.Token accessToken = authTokenService.generateJwtToken(request.getUsername());
        AuthTokenService.Token refreshToken = authTokenService.generateRefreshToken(request.getUsername());

        log.info("accessToken: {} refreshToken: {}",accessToken.getToken(), refreshToken.getToken());

        return LoginResponse.builder()
                .accessToken(accessToken.getToken())
                .accessTokenExpiresIn(accessToken.getExpiresIn())
                .refreshToken(refreshToken.getToken())
                .roles(Set.of("user"))
                .build();
    }
}
