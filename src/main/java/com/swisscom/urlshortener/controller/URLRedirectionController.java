package com.swisscom.urlshortener.controller;

import com.swisscom.urlshortener.util.UrlMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@Slf4j
@RestController
@RequiredArgsConstructor
public class URLRedirectionController {

    private final UrlMapper urlMapper;

    @GetMapping("/{shortUrl}")
    public RedirectView redirectToOriginalUrl(@PathVariable("shortUrl") String shortUrl) {
        String originalUrl = urlMapper.getOriginalUrl(shortUrl);
        return new RedirectView(originalUrl);
    }
}
