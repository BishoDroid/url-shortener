package com.swisscom.urlshortener.controller;

import com.swisscom.urlshortener.dto.request.BaseRequest;
import com.swisscom.urlshortener.dto.request.ShortenUrlRequest;
import com.swisscom.urlshortener.dto.request.UrlDetailsRequest;
import com.swisscom.urlshortener.dto.request.UrlStatusRequest;
import com.swisscom.urlshortener.dto.response.AllUrlDetailsResponse;
import com.swisscom.urlshortener.dto.response.ShortenUrlResponse;
import com.swisscom.urlshortener.dto.response.UrlDetailsResponse;
import com.swisscom.urlshortener.dto.response.UrlStatusResponse;
import com.swisscom.urlshortener.usecase.AllUrlDetailsUseCase;
import com.swisscom.urlshortener.usecase.ShortenUrlUseCase;
import com.swisscom.urlshortener.usecase.UrlDetailsUseCase;
import com.swisscom.urlshortener.usecase.UrlStatusUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/urls")
@RequiredArgsConstructor
public class UrlsController {

    private final UrlStatusUseCase urlStatusUseCase;
    private final ShortenUrlUseCase shortenUrlUseCase;
    private final UrlDetailsUseCase urlDetailsUseCase;
    private final AllUrlDetailsUseCase allUrlDetailsUseCase;

    @PostMapping("/shorten")
    public ResponseEntity<ShortenUrlResponse> shortenUrl(@RequestBody ShortenUrlRequest request) {
        return ResponseEntity.ok(shortenUrlUseCase.execute(request));
    }

    @PutMapping("/activate")
    public ResponseEntity<UrlStatusResponse> activateShortUrl(@RequestBody UrlStatusRequest request) {
        return processUrlStatusRequest(request);
    }

    @PutMapping("/deactivate")
    public ResponseEntity<UrlStatusResponse> deactivateShortUrl(@RequestBody UrlStatusRequest request) {
        return processUrlStatusRequest(request);
    }

    @GetMapping("/details")
    public ResponseEntity<AllUrlDetailsResponse> getAllUrlsDetails() {
        return ResponseEntity.ok(allUrlDetailsUseCase.execute(new BaseRequest(null)));
    }

    @GetMapping("/details/{shortUrl}")
    public ResponseEntity<UrlDetailsResponse> getUrlDetails(@PathVariable("shortUrl") String shortUrl) {
        UrlDetailsRequest request = new UrlDetailsRequest(null, shortUrl);
        return ResponseEntity.ok(urlDetailsUseCase.execute(request));
    }

    private ResponseEntity<UrlStatusResponse> processUrlStatusRequest(UrlStatusRequest request) {
        UrlStatusResponse response = urlStatusUseCase.execute(request);
        return ResponseEntity.status(response.getStatus().value()).body(response);
    }
}
