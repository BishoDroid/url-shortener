package com.swisscom.urlshortener.controller;

import com.swisscom.urlshortener.dto.request.LoginRequest;
import com.swisscom.urlshortener.dto.request.RefreshTokenRequest;
import com.swisscom.urlshortener.dto.request.RegistrationRequest;
import com.swisscom.urlshortener.dto.response.RefreshTokenResponse;
import com.swisscom.urlshortener.dto.response.LoginResponse;
import com.swisscom.urlshortener.dto.response.RegistrationResponse;
import com.swisscom.urlshortener.usecase.LoginUseCase;
import com.swisscom.urlshortener.usecase.RefreshTokenUseCase;
import com.swisscom.urlshortener.usecase.RegistrationUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AutController {

    private final LoginUseCase loginUseCase;
    private final RegistrationUseCase registrationUseCase;
    private final RefreshTokenUseCase refreshTokenUseCase;

    @PostMapping("/register")
    public ResponseEntity<RegistrationResponse> register(@RequestBody RegistrationRequest requestBody) {
        return ResponseEntity.ok(registrationUseCase.execute(requestBody));
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest requestBody) {
        return ResponseEntity.ok(loginUseCase.execute(requestBody));
    }

    @PostMapping("/refresh")
    public ResponseEntity<RefreshTokenResponse> refresh(@RequestBody RefreshTokenRequest request) {
        return ResponseEntity.ok(refreshTokenUseCase.execute(request));
    }
}
