package com.swisscom.urlshortener.dto.response;


import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

@Data
@SuperBuilder
public class BaseResponse {

    HttpStatus status;
    String message;
}
