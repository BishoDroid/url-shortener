package com.swisscom.urlshortener.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.swisscom.urlshortener.model.User;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

@Value
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@EqualsAndHashCode(callSuper = true)
public class RegistrationResponse extends BaseResponse {

    String userId;
    String email;
    String username;

    public static RegistrationResponse fromUser(User user) {
        return RegistrationResponse.builder()
                .status(HttpStatus.OK)
                .message("User created successfully")
                .userId(user.getId())
                .email(user.getEmail())
                .username(user.getUsername())
                .build();
    }
}
