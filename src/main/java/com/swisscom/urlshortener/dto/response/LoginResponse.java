package com.swisscom.urlshortener.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.Set;

@Value
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@EqualsAndHashCode(callSuper = true)
public class LoginResponse extends BaseResponse {

    String accessToken;
    Instant accessTokenExpiresIn;
    String refreshToken;
    Set<String> roles;
    String tokenType = "Bearer";
}
