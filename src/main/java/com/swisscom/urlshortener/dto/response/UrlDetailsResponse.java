package com.swisscom.urlshortener.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.util.UrlShortener;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

@Value
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UrlDetailsResponse extends BaseResponse {
    String shortUrl;
    String originalUrl;
    long clicks;

    public static UrlDetailsResponse fromModel(Url url) {
        return UrlDetailsResponse.builder()
                .shortUrl(url.getPrefixedUrl())
                .originalUrl(url.getLongUrl())
                .clicks(url.getClicks())
                .build();
    }
}
