package com.swisscom.urlshortener.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.swisscom.urlshortener.dto.response.BaseResponse;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Value
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@EqualsAndHashCode(callSuper = true)
public class RefreshTokenResponse extends BaseResponse {

    String accessToken;
    Instant accessTokenExpiresIn;
    String refreshToken;
    Instant refreshTokenExpiresIn;
    String tokenType = "Bearer";
}
