package com.swisscom.urlshortener.dto.request;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class UrlDetailsRequest extends BaseRequest{
    String shortUrl;

    public UrlDetailsRequest(String requestId, String shortUrl) {
        super(requestId);
        this.shortUrl = shortUrl;
    }
}
