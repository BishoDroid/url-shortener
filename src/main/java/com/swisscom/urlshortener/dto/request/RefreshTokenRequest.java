package com.swisscom.urlshortener.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = false)
public class RefreshTokenRequest extends BaseRequest {

    @NotBlank String refreshToken;

    public RefreshTokenRequest(String requestId, String refreshToken) {
        super(requestId);
        this.refreshToken = refreshToken;
    }
}
