package com.swisscom.urlshortener.dto.request;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class LoginRequest extends BaseRequest {

    String username;
    String password;
    public LoginRequest(String requestId, @NonNull String username, String password) {
        super(requestId);
        this.username = username;
        this.password = password;
    }
}
