package com.swisscom.urlshortener.dto.request;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class ShortenUrlRequest extends BaseRequest {
    String url;
    String username;

    public ShortenUrlRequest(String requestId, @NonNull String url, @NonNull String username) {
        super(requestId);
        this.url = url;
        this.username = username;
    }
}
