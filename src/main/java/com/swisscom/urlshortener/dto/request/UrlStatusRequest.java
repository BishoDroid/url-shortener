package com.swisscom.urlshortener.dto.request;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class UrlStatusRequest extends BaseRequest{
    boolean isActive;
    String shortUrl;

    public UrlStatusRequest(String requestId, boolean isActive, String shortUrl) {
        super(requestId);
        this.isActive = isActive;
        this.shortUrl = shortUrl;
    }
}
