package com.swisscom.urlshortener.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class RegistrationRequest extends BaseRequest {

    @NotBlank
    @Email
    @Size(min = 8, max = 64)
    String email;

    @NotBlank
    @Size(min = 8, max = 16)
    String username;

    @NotBlank
    @Size(min = 8, max = 64)
    String password;

    public RegistrationRequest(String requestId, String username, String email, String password) {
        super(requestId);
        this.email = email;
        this.username = username;
        this.password = password;
    }
}
