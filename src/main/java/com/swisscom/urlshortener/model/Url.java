package com.swisscom.urlshortener.model;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "urls")
@EqualsAndHashCode(callSuper = true)
public class Url extends BaseModel {

    @NotBlank
    @Indexed(unique = true)
    private String shortUrl;

    @NotBlank
    private String longUrl;

    private long clicks;

    private boolean active;

    private String prefix;

    @DBRef
    private User user;

    public void incrementClicks() {
        ++clicks;
    }

    public boolean switchUrl() {
        this.active = !active;
        return this.active;
    }

    public String getPrefixedUrl() {
        return getPrefix() + getShortUrl();
    }
}
