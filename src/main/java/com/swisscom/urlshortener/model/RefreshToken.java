package com.swisscom.urlshortener.model;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@Document(collection = "refreshToken")
public class RefreshToken extends BaseModel {

    @NotBlank
    private String token;
    @DBRef
    private User user;
    private Instant expiryDate;
}
