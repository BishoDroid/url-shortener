package com.swisscom.urlshortener.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Data
@Builder
@Document(collection = "users")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseModel {

    private String email;
    @Indexed(unique = true)
    private String username;
    private String password;

    private Set<String> roles;
}
