package com.swisscom.urlshortener.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.Instant;

@Getter
public class BaseModel {

    @Id
    @Indexed(unique = true)
    String id;
    Instant createdAt;

    @LastModifiedDate
    @EqualsAndHashCode.Exclude
    Instant updatedAt;

    public BaseModel() {
        this.id = new ObjectId().toHexString();
        this.createdAt = Instant.now();
    }
}
