package com.swisscom.urlshortener.repository;

import com.swisscom.urlshortener.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String userName);
    Boolean existsByEmail(String email);
}
