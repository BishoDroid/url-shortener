package com.swisscom.urlshortener.repository;

import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UrlRepository extends MongoRepository<Url, String> {

    Optional<List<Url>> findByUser(User user);

    Optional<Url> findOneByShortUrl(String shortUrl);
    boolean existsByShortUrl(String shortUrl);
}
