package com.swisscom.urlshortener.auth;

import com.swisscom.urlshortener.config.AppConfig;
import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.repository.RefreshTokenRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthTokenService {

    private final AppConfig appConfig;
    private final RefreshTokenRepository refreshTokenRepository;

    public Token generateJwtToken(String user) {
        return generateToken(user, appConfig.getJwtSecret(), appConfig.getJwtExpiration());
    }

    public  Token generateRefreshToken(String user) {
        return generateToken(user, appConfig.getJwtRefreshSecret(), appConfig.getJwtRefreshExpiration());
    }

    public void deleteRefreshToken(String refreshToken) {
        refreshTokenRepository.deleteByToken(refreshToken);
    }

    public Token generateFromRefreshToken(String refreshToken) {
        log.info("Validating refresh token");
        Claims claims;
        if(validateRefreshToken(refreshToken)) {
            claims = Jwts.parser()
                    .setSigningKey(appConfig.getJwtRefreshSecret())
                    .parseClaimsJws(refreshToken)
                    .getBody();
            String username = claims.getSubject();
            if (username == null || username.isEmpty()) {
                throw new AppException(AppError.INVALID_TOKEN_SUBJECT);
            }
            return generateToken(claims.getSubject(), appConfig.getJwtSecret(), appConfig.getJwtExpiration());
        }
        throw new AppException(AppError.INVALID_TOKEN);
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(appConfig.getJwtSecret())
                .parseClaimsJws(token)
                .getBody();

        // Extract username and roles/authorities from the token
        String username = claims.getSubject();
        Collection<? extends SimpleGrantedAuthority> authorities =
                Stream.of("user")
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        // Create UserDetails and Authentication objects
        UserDetails userDetails = new User(username, "", authorities);
        return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);

    }

    public boolean validateRefreshToken(String refreshToken) {
        return validateToken(refreshToken, appConfig.getJwtRefreshSecret());
    }

    public boolean refreshTokenExists(String refreshToken) throws AppException {
        return refreshTokenRepository.findByToken(refreshToken).isEmpty();
    }
    public boolean validateJwtToken(String authToken) {
        return validateToken(authToken, appConfig.getJwtSecret());
    }

    private boolean validateToken(String token, String jwtSecret) throws AppException{
        try {
            Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
            return true;
        } catch (ExpiredJwtException e) {
            log.error("Token expired. {}", e.getMessage());
            throw new AppException(AppError.EXPIRED_TOKEN);
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException e) {
            log.error("Token invalid. {}", e.getMessage());
            throw new AppException(AppError.INVALID_TOKEN);
        } catch (JwtException e) {
            log.error("An error occurred while parsing token");
            throw new AppException(AppError.INVALID_TOKEN);
        }
    }

    private Token generateToken(String username, String secret, int expiration) {
        Instant exp = calculateExpirationTime(expiration);
        String token = Jwts.builder()
                .setSubject((username))
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(exp))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
        return Token.builder().token(token).expiresIn(exp).build();
    }

    private Instant calculateExpirationTime(int expirationMinutes) {
        return Instant.now().plus(Duration.ofMinutes(expirationMinutes));
    }

    @Value
    @Builder
    public static class Token {
        String token;
        Instant expiresIn;
    }
}
