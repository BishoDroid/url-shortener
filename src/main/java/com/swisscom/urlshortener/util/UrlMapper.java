package com.swisscom.urlshortener.util;

import com.swisscom.urlshortener.exception.AppError;
import com.swisscom.urlshortener.exception.AppException;
import com.swisscom.urlshortener.model.Url;
import com.swisscom.urlshortener.repository.UrlRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class UrlMapper {

    private final UrlRepository urlRepository;

    public String getOriginalUrl(String shortUrl) {
        log.info("Mapping short url {} to its original value", shortUrl);
        Url url = urlRepository.findOneByShortUrl(shortUrl)
                .orElseThrow(() -> new AppException(AppError.URL_NOT_FOUND));
        if(!url.isActive()) {
            throw new AppException(AppError.URL_NOT_ACTIVE);
        }
        url.incrementClicks();
        urlRepository.save(url);
        return url.getLongUrl();
    }

}
