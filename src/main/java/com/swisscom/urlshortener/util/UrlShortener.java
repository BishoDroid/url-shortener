package com.swisscom.urlshortener.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Component
public class UrlShortener {

    @Value("${app.prefix}")
    private String prefix;

    public String shortenUrl(String url, long counter) throws NoSuchAlgorithmException {
        String combinedInput = url + counter;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hash = messageDigest.digest(combinedInput.getBytes(StandardCharsets.UTF_8));
        String shortUrl = toHexString(hash).substring(0, 8);
        log.error("Original url: {}. Short url: {}", url, shortUrl);
        return shortUrl;
    }

    public  String prefixedShortUrl(String shortUrl) {
        return prefix + shortUrl;
    }

    private  String toHexString(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
