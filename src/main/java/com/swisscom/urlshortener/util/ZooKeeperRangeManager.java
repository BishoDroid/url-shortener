package com.swisscom.urlshortener.util;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
public class ZooKeeperRangeManager {

    private static final long RANGE_SIZE = 10_000_000_000L;
    private final ZooKeeper zooKeeper;
    private final String rangeBasePath;
    private final AtomicLong currentCounter = new AtomicLong();
    private long rangeEnd;

    public ZooKeeperRangeManager(ZooKeeper zooKeeper, @Value("${app.rangeBasePath}") String rangeBasePath) {
        this.zooKeeper = zooKeeper;
        this.rangeBasePath = rangeBasePath;
    }

    @PostConstruct
    public void init() throws Exception {
        createZNodeIfNeeded(rangeBasePath);
        String serverNodePath = obtainServerNodePath();
        assignRangeToServer(serverNodePath);
    }

    private String obtainServerNodePath() {
        return rangeBasePath + "/" + UUID.randomUUID();
    }

    private void assignRangeToServer(String serverNodePath) throws Exception {
        List<String> children = zooKeeper.getChildren(rangeBasePath, false);
        long maxRangeEnd = children.stream()
                .map(child -> {
                    try {
                        return new String(zooKeeper.getData(rangeBasePath + "/" + child, false, null));
                    } catch (KeeperException | InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                })
                .mapToLong(rangeData -> Long.parseLong(rangeData.split("-")[1]))
                .max()
                .orElse(0L);

        this.currentCounter.set(maxRangeEnd + 1);
        this.rangeEnd = maxRangeEnd + RANGE_SIZE;

        log.info("Assigning new range to server {}", serverNodePath);
        zooKeeper.create(serverNodePath, (currentCounter + "-" + rangeEnd).getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
    }

    public void createZNodeIfNeeded(String path) {
        try {
            if (zooKeeper.exists(path, false) == null) {
                zooKeeper.create(path, new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error creating znode in ZooKeeper", e);
        }
    }

    public long getNextCounter() {
        long nextCounter = this.currentCounter.getAndIncrement();
        if (nextCounter >= rangeEnd) {
            throw new RuntimeException("Counter range exhausted for this server.");
        }
        log.info("New counter retrieved from zookeeper: {}", nextCounter);
        return nextCounter;
    }
}
